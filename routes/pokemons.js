var express = require('express');
var router = express.Router();
var path = require('path');
var bodyParser = require('body-parser');
var parseUrlencoded = bodyParser.urlencoded({extended:false});
var pokemons={
  'Charmander': 'The flame on its tail indicates Charmander life force. If it is healthy, the flame burns brightly.',
  'Squirtle': 'It shelters itself in its shell, then strikes back with spouts of water at every opportunity.',
  'Bulbasaur': 'A strange seed was planted on its back at birth. The plant sprouts and grows with this Pokémon.'
}

var name, description, pokemon;
// router.use('/pokemons',express.static('main'));
router.route('/')
  .get(function (req, res){
    res.render('pokemons');
    // var uid = req.params.uid,
    //     path = req.params[0] ? req.params[0] : 'index.html';
    // res.sendfile(path,{root: './main'});
});


router.route('/list')
  .get(function(req, res){
    // var pokemons=['Charmander', 'Squirtle', 'Bulbasaur'];
    //
    // if(req.query.limit>=0){
    //   res.json(pokemons.slice(0, req.query.limit));
    // }else{
    //   res.json(pokemons);
    // }
    res.json(Object.keys(pokemons));
  })
  .post(parseUrlencoded, function(req, res){
    var newPokemon = req.body;
    pokemons[newPokemon.name] = newPokemon.description;
    res.status(201).json(newPokemon.name);
  });

router.route('/:name')
  .all(function(req, res, next){
    name = req.params.name;
    pokemon = name[0].toUpperCase() + name.slice(1).toLowerCase();
    req.pokemonName = pokemon;

    next();
  })
  .get(function(req, res){
    description = pokemons[req.pokemonName];

    if(!description){
      res.status(404).json('No pokemon found for '+ req.params.name);
    }else{
      res.json(description);
    }
  })

  .delete(function(req, res){
    delete pokemons[req.pokemonName];
    res.sendStatus(200);
  });

module.exports = router;
