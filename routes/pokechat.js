var express = require('express');
var pokechat = express.Router();
var geolib = require('geolib');
var bodyParser = require('body-parser');
var parseUrlencoded = bodyParser.urlencoded({extended:false});

var spots = {
  "Arima": {latitude: 10.614718, longitude: -61.271439},
  "Chaguanas": {latitude: 10.505366, longitude: -61.396408},
  "Point Fortin": {latitude: 10.162209, longitude: -61.666946},
  "Port of Spain": {latitude: 10.654901 , longitude: -61.501926},
  "Sande Grande": {latitude: 10.582322, longitude: -61.131363},
  "San Fernando": {latitude: 10.285194, longitude:  -61.436234},
  "Toco": {latitude: 10.822515, longitude: -60.940475}
}
// var currentLocation = {latitude:  10.210868, longitude: -61.257706};

pokechat.route('/')
  .get(function( req, res){
    res.render('pokechat');
  });

pokechat.route('/locations')
  .get(function(req, res){
    res.json(Object.keys(spots));
    console.log(spots);
});

// pokechat.route('/locations/nearest')
//   .get(function(req, res){
//     var nearestSpot = geolib.findNearest(currentLocation, spots, 0);
//     res.json(nearestSpot.key);
//     console.log(nearestSpot);
//   });

pokechat.route('/locations/nearest')
  .get(function(req, res){
    currLat = req.query.lat;
    currLon = req.query.lon;
    currentLocation = {latitude:  currLat, longitude: currLon};
    var nearSpots =  geolib.orderByDistance(currentLocation,spots);
    var sortedLocs=[];
    for(i = 0; i<nearSpots.length; i++){
      sortedLocs[i] = nearSpots[i].key;
    }
    res.json(sortedLocs);
    console.log(sortedLocs);
  });

module.exports = pokechat;
