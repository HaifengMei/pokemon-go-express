exports = module.exports = function(io){
  var redis = require('redis');
  var redisClient = redis.createClient();
  var messages = [];
  var names =[];

  var storeMessage = function(name, data){
    var message = JSON.stringify({name: name, data: data});
    redisClient.lpush("messages", message, function(err, response){
      redisClient.ltrim("messages", 0 ,9);
    });
  }


  io.sockets.on('connection', function (client) {
    client.on('join', function(name){
      console.log(name+' connected');
      client.nickname=name;
      client.broadcast.emit("join", name + " joined the chat");
      client.broadcast.emit("add member", name);
      redisClient.sadd("members", name);
      redisClient.smembers('members', function(err, names){
        names.forEach(function(name){
          client.emit('add member', name);
        });
      });

      redisClient.lrange("messages", 0, -1, function(err, messages){
        messages = messages.reverse();
        messages.forEach(function(message){
          message = JSON.parse(message);
          client.emit("messages", message.name+ ": " + message.data);
        });
      });
    });

    client.on('messages', function(message){
      var nickname= client.nickname;
      client.broadcast.emit("messages", nickname + ": " + message);
      client.emit("messages", nickname + ": " + message);
      storeMessage(client.nickname, message);
      console.log(nickname + ": " + message);
    });

    client.on('disconnect', function(){
      if(client.nickname!=null){
        console.log(client.nickname+' disconnected');
        client.broadcast.emit("remove member", client.nickname);
        redisClient.srem("members", client.nickname);
      }
    });
  });
}
