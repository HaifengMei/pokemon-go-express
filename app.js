"use strict";

var express = require('express'),
  app = express(),
  logger = require('./logger'),
  hbs = require('hbs'),
  path = require('path'),
  server = require('http').createServer(app),
  io = require('socket.io')(server),
  socket = require('./routes/socket')(io),
  config = require('./config');

// app.use(logger);


app.set('view engine', 'hbs');
app.set('views', __dirname + '/main');
app.engine('html', hbs.__express);
app.use(express.static(path.join(__dirname, 'main')));

hbs.registerPartials(__dirname + '/main/partials');

var index = require('./routes/index');
var pokemons = require('./routes/pokemons');
var pokechat = require('./routes/pokechat');
var teams = require('./routes/teams');
app.use('/', index);
app.use('/pokemons', pokemons);
app.use('/pokechat', pokechat);
app.use('/teams',teams);



server.listen(config.port, function(){
  console.log('Listening on on port' +config.port+' of '+ config.hostname);
});
