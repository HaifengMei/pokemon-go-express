$(function(){
  //Retrieved list of pokemons
  $.get('/pokemons/list', appendToList);

  //sPost new pokemon details
  $('form').on('submit', function(event){
    event.preventDefault();
    var form =$(this);
    var pokemonData = form.serialize();

    $.ajax({
      type: 'POST', url: '/pokemons/list', data: pokemonData
    }).done(function(pokemonName){
      appendToList([pokemonName]);
      form.trigger('reset');
    });
  });

  // Add a new pokemon to list
  function appendToList(pokemons){
    var list=[];
    var content, pokemon;
    for(var i in pokemons){
      pokemon = pokemons[i];
      content = '<a href="#" data-pokemon="'+pokemon+'"><img class="delete-icon" src="/assets/i/delete.png"></a>'+
      '<a href="/pokemons/'+pokemon+'">'+pokemon+'</a>';
      list.push($('<li>', {html: content}));

      $('.pokemon-list').append(list);
    }
  }

  //delete pokemon from list
  $('.pokemon-list').on('click', 'a[data-pokemon]', function(event){
    if(!confirm('Are you sure?')){
      return false;
    }

    var target = $(event.currentTarget);
    $.ajax({
      type: 'DELETE', url:'/pokemons/' + target.data('pokemon')
    }).done(function(){
      target.parents('li').remove();
    });
  });

  socket.on('connect', function(data){
    $('#status').html('Connected to PokeChat');
    nickname = prompt("What is your tainer name?");
    if(nickname==""||nickname==null){
      nickname = "Trainer"+Math.floor((Math.random() * 100) + 1);
    }
    // nickname = "Trainer"+Math.floor((Math.random() * 100) + 1);
    socket.emit('join', nickname);

  });

  $('.pokechat__form').submit(function(e){
    var message = $('.pokechat__input').val();
    socket.emit('messages', message);
    $('.pokechat__input').val('');
    return false;
  });

  socket.on('messages', function(msg){
    var content = msg.split(":");
    var name = content[0];
    var text = content[1];
    var d = new Date(),
      h = (d.getHours()<10?'0':'') + d.getHours(),
      m = (d.getMinutes()<10?'0':'') + d.getMinutes();
    // var time = h + ':' + m;
    var time;
    if(h>12){
      time = h-12 + ':' + m + " PM";
    }else if(h<12){
      time = h + ':' + m + " AM";
    }else{
      time = 12 + ':' + m + " PM";
    }
    $('.messages').append('<li class="row"><div class="messages--name col-xs-12">'+name+'</div> <div class="messages--text col-xs-10">'+text+'</div> <div class="messages--time">'+time+'</div></li>');
    $('.pokechat__console--right').animate({scrollTop: $('.pokechat__console--right').prop("scrollHeight")}, 600);
  });

  socket.on('join', function(msg){
    $('.messages').append($('<li class="join-left">').text(msg));
    $('.pokechat__console--right').animate({scrollTop: $('.pokechat__console--right').prop("scrollHeight")}, 600);

  });

  socket.on('add member', function(name){
    var member = $('<li><img class ="online" src="/assets/i/online.png">'+name+'</li>').data('name', name);
    $('.members').append(member);
    $('.pokechat__console--right').animate({scrollTop: $('.pokechat__console--right').prop("scrollHeight")}, 600);

  });

  socket.on('remove member', function(name){
    console.log(name);
    $('.messages').append($('<li class="join-left">').text(name+" left the room"));
    $('.members li:contains("'+name+'")').remove();
    $('.pokechat__console--right').animate({scrollTop: $('.pokechat__console--right').prop("scrollHeight")}, 600);

  });

});
