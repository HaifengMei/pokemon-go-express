module.exports = function(grunt) {
  grunt.initConfig({
  pkg: grunt.file.readJSON('package.json'),
  uglify: {
    options: {
      banner: '/* Pokemon GO Express \n' +
        ' * ======\n' +
        ' * @license: CC BY-NC 3.0 (http://creativecommons.org/licenses/by-nc/3.0/)\n' +
        ' * @author: Haifeng Mei. <haifengmei@hotmail.com>\n' +
        ' * @updated: ' + (new Date()).toDateString() + '\n' +
        ' */\n'
    },
    my_target: {
        files: { 'main/assets/js/app.min.js' : ['main/assets/js/app.merged.js'] }
      }
  },

  concat: {
      options: {
        sourceMap: true,
        stripBanners: true,
        // banner: '/* Pokemon GO Express \n' +
        //   ' * ======\n' +
        //   ' * @license: CC BY-NC 3.0 (http://creativecommons.org/licenses/by-nc/3.0/)\n' +
        //   ' * @author: Haifeng Mei. <haifengmei@hotmail.com>\n' +
        //   ' * @updated: ' + (new Date()).toDateString() + '\n' +
        //   ' */\n'
      },
      dist: {
        src: [
          'main/src/components/jquery/dist/jquery.js',
          'main/src/components/bootstrap-sass/assets/javascripts/bootstrap.js',
          // 'main/node_modules/socket.io/node_modules/socket.io-client/socket.io.js',
          'main/src/js/*.js'
          ],
        dest: 'main/assets/js/app.merged.js'
      }
    },

    sass: {
      dev: {
        options: {
          style: 'expanded',
          sourcemap: true,
          // banner: '/* Pokemon GO Express \n' +
          //   ' * ======\n' +
          //   ' * @license: CC BY-NC 3.0 (http://creativecommons.org/licenses/by-nc/3.0/)\n' +
          //   ' * @author: Haifeng Mei. <haifengmei@hotmail.com>\n' +
          //   ' * @updated: ' + (new Date()).toDateString() + '\n' +
          //   ' */\n'
        },
        files: {
          'main/assets/css/master.css': 'main/src/sass/master.scss'
        }
      },
      dist: {
        options: {
          style: 'compressed',
          sourcemap: false,
          banner: '/* Pokemon GO Express \n' +
            ' * ======\n' +
            ' * @license: CC BY-NC 3.0 (http://creativecommons.org/licenses/by-nc/3.0/)\n' +
            ' * @author: Haifeng Mei. <haifengmei@hotmail.com>\n' +
            ' * @updated: ' + (new Date()).toDateString() + '\n' +
            ' */\n'
        },
        files: {
          'main/assets/css/master.css': 'main/src/sass/master.scss'
        }
      }
    },

    postcss: {
      options: {
        map: true, // inline sourcemaps

        // or
        map: {
            inline: false, // save all sourcemaps as separate files...
            annotation: 'dist/css/maps/' // ...to the specified directory
        },

        processors: [
          require('pixrem')(), // add fallbacks for rem units
          require('autoprefixer')(
            {
              browsers:[
                "Android 2.3",
                "Android >= 4",
                "Chrome >= 20",
                "Firefox >= 24",
                "Explorer >= 8",
                "iOS >= 6",
                "Opera >= 12",
                "Safari >= 6"
              ]
              // browsers: ['last 2 versions']
            }
          ), // add vendor prefixes
          require('cssnano')() // minify the result
        ]
      },
      dist: {
        files: {
          'main/assets/css/master.css': 'main/src/sass/master.scss'
        }
      }
    },

    watch: {
  	  sass: {
        files: ['main/src/sass/*.scss'],
  	    tasks: ['sass:dev']
  	  },
  	  js: {
        files: ['main/src/js/*.js'],
  	    tasks: ['concat:dist']
  	  },
      configFiles: {
        files: ['Gruntfile.js'],
        options: {
          reload: true
        }
      }
  	}

  });

  grunt.registerTask('default', ['postcss:dist','sass:dev', 'concat:dist' , 'watch']);
  grunt.registerTask('build', ['postcss:dist','sass:dist', 'concat:dist']);
  grunt.registerTask('deploy', ['postcss:dist','sass:dist', , 'concat:dist']);

  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');

};
