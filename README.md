# PokeExpress #

Pokemon GO Express(PokeExpress) is an website based around the recently launched [Pokemon GO](http://www.pokemongo.com/en-us/) for android and ios devices.Pokemon GO Express was developed with the purpose of facilitating communication between players and team members which the app lacks. In order in improve the game experience and share game knowledge between players, PokeExpress provide the following features:

1) GO Chat - A real time chat room which serve as the primary means of communication between players. Chat rooms fall under 3 categories: Team Based, All chat and Location based. (Under development)

2) GO Portal - A Pokemon GO portal with the latest Pokemon GO news

3) GO Team - A portal for the respective Pokemon GO team you joined. It facilitate  creation of sub teams and dedicated leaders to schedule and organize team meets up and events. Team members can also make posts for discussion.

4) GO Catch - A directory of all Pokemons you can catch in Pokemon GO, their expected location, techniques and any related information that can help you advance in the game and become a Pokemon master.